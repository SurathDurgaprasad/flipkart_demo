package com.flipkart;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC01_AddProduct extends ResusableMethods{


	@BeforeTest
	public void LaunchAndLogintoApplication() throws Exception  {
		openApplicationAndTitleValidation(url, TitleOfApplication);
		Screenshot_method("openApplication");
		closeLoginPopupWizard();		
	} 

	@Test
	public  void AddProductToCart() throws Exception  {
	search("Iphone 13");
	Screenshot_method("Iphone 13");
	Thread.sleep(5000);
	selectMobile();	
	Thread.sleep(5000);
	TabTitle();
	Screenshot_method(MobilesDescription);
	System.out.println(driver.getWindowHandles().size());
	if (driver.getWindowHandles().size() == 2) {
		handleTabs(1);
		clickAddtoCart();
		Screenshot_method("AddtoCart");
		TabTitle();
		Thread.sleep(1500);
		validateMessage();
		Screenshot_method("User Message");
	}
	else {
		System.out.println("The desired mobile is failed to open in the New Tab.");
	}

	} 

	@AfterMethod
	public void ValidatePaymentMethods() throws Exception  {	
		closeBrowser();	
	}

}
