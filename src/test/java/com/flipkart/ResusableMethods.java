package com.flipkart;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ResusableMethods extends Locators{

	public void Screenshot_method(String screenshotName) {
		TakesScreenshot scrShot =((TakesScreenshot) driver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(SrcFile	, new File(screenshotBaseFilePath+screenshotName+extension));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public void TabTitle() {
		System.out.println(driver.getTitle());
	}
	
	public void openApplicationAndTitleValidation(String ApplicationUrl, String ApplicationTitle) {
		ChromeOptions options = new ChromeOptions().addArguments("--headless=new");
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.navigate().to(ApplicationUrl);
		if (driver.getTitle().contains(ApplicationTitle)){	
			System.out.println("Application is opened successfully.");
		}
		else {
			System.out.println("Application is failed to open.");	
		}
	}

	public void closeLoginPopupWizard() {
		if (driver.findElement(By.xpath(LoginWizardValidation)).isDisplayed()) {
			System.out.println("Default Login Wizard is displayed.");
			driver.findElement(By.xpath(LoginWizardCloseButton)).click();
		}
		else {
			System.out.println("Default Login Wizard is not displayed.");	
		}
	}

	public void closeBrowser() {
		driver.quit();
	}

	public void search(String searchword) {
		driver.findElement(By.xpath(SearchInput)).sendKeys(searchword);
		driver.findElement(By.xpath(SearchButton)).click();
	}

	public void selectMobile() throws AWTException {
		MobilesDescriptionSectionCount = driver.findElements(By.xpath(MobilesDescriptionSection)).size();
		System.out.println("Number of Mobiles displayed in the page: "+MobilesDescriptionSectionCount);
		for(int i = 2; i<= MobilesDescriptionSectionCount; i++) {
			Description = driver.findElement(By.xpath("//*[@id=\'container\']/div/div[3]/div[1]/div[2]/div["+i+"]/div/div/div/a/div[2]/div[1]/div[1]")).getText();
			if (Description.equalsIgnoreCase(MobilesDescription)) {
				System.out.println("The expected modile is shown in the List");		    
				driver.findElement(By.xpath("//*[@id=\'container\']/div/div[3]/div[1]/div[2]/div["+i+"]/div/div/div/a/div[2]/div[1]/div[1]")).click();
				break;
			}	
		}	
	}
	
	public void handleTabs(int tabNumber) {
		wind = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(wind);
		driver.switchTo().window(wind.get(tabNumber));
	}

	public void clickAddtoCart() {
		driver.findElement(By.xpath(AddtoCart)).click();
	}

	public void validateMessage() {
		if(driver.findElement(By.xpath(SorryMessage))!= null) {
			System.out.println("A Message is dispalyed saying We're sorry! Only 1 unit(s) for each customer");
		}
	}
}
