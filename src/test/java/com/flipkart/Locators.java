package com.flipkart;

public class Locators extends VariablesData{

	public String LoginWizardValidation = "//*[text()='Login']";
	
	public String LoginWizardCloseButton = "//*[@class='_2KpZ6l _2doB4z']";
	
	public String SearchInput = "//input[@type='text']//self::input";

	public String SearchButton = "//*[@type='submit']";
	
	public String MobilesDescriptionSection ="//*[@id=\'container\']/div/div[3]/div[1]/div[2]/div/div/div/div/a/div[2]";
	
	public String AddtoCart = "//*[text()='Add to cart']";
	
	public String SorryMessage = "//*[@class='_2sKwjB']";
}
