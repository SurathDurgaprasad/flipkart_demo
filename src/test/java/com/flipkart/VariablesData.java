package com.flipkart;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class VariablesData {

	public static WebDriver driver = null;

	//public String screenshotBaseFilePath = "C:\\Users\\admin\\Documents\\Eclipse_Workspace\\Xpath\\ScreenShotFolder\\";
	
	public String screenshotBaseFilePath = "/src/ScreenShots/";

	public  ArrayList<String> wind;
	
	public String extension = ".jpg";

	public String url = "https://www.flipkart.com/";

	public String TitleOfApplication = "Online Shopping";

	public int MobilesDescriptionSectionCount;

	public String MobilesDescription = "APPLE iPhone 13 (Starlight, 512 GB)";
	
	public String Description;
	
	public WebDriverWait wait;
}